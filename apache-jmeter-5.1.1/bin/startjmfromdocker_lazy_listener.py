import xml.etree.ElementTree as ET

listenerTree = ET.parse("/listener_script.jmx")
listenerRoot= listenerTree.getroot()
scriptTree = ET.parse('/base_script.jmx')
scriptRoot = scriptTree.getroot()

# disable all listeners in script
for element in scriptRoot.iter("BackendListener"):
    element.set('enabled', 'false')
for element in scriptRoot.iter("ResultCollector"):
    element.set('enabled', 'false')

# attach predefined listener (path should be fixed)
for node in listenerRoot.iter("BackendListener"):
    scriptRoot.find('hashTree/hashTree').append(node)

scriptTree.write('/script.jmx')